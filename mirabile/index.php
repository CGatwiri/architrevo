<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Mirabile Archdesigns</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

	<link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/cards.css">
	<link rel="stylesheet" href="css/hover.css">
	<link rel="stylesheet" href="css/fade.css">
	<link rel="stylesheet" href="css/myside.css">
	<link rel="stylesheet" href="css/side.css">
	<link rel="stylesheet" href="css/polaroid.css">
	<link rel="stylesheet" href="css/imagegrid.css">
	<link rel="stylesheet" href="css/image2.css">

	<style>
		.sticky
		{
			position: fixed;
			top: 0;
  			width: 100%;
		}
	</style>
	
  </head>

  <body>
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
          <div class="container">
            <img src="images/logos/archi1.png" style="height:200px;width:230px;display:flex;"></img> &nbsp;&nbsp;
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
          </button>

	      <div class="collapse navbar-collapse" id="ftco-nav" style="font-family: Poppins;">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a href="index.php" class="nav-link">Home</a></li>
				<li class="nav-item"><a href="about.html" class="nav-link">About Us</a></li>
				<li class="nav-item"><a href="services.html" class="nav-link">Our Services</a></li>
				<li class="nav-item"><a href="portfolio.html" class="nav-link">Our Portfolio</a></li>
				<li class="nav-item"><a href="house_plans.html" class="nav-link">House Designs</a></li>
				<li class="nav-item"><a href="blog.php" class="nav-link">News and Insights</a></li>
				<li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>
			  </ul>
	      </div>
	    </div>
	</nav>
    
    <div class="hero-wrap js-fullheight" style="background-image: url('images/slideshow/back1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-10 text-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
          	<h2 class="subheading" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Welcome!</h2>
            <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">
            	<span>Inspiring Better Living</span>
            </h1>
            <!-- <p><a href="#" class="btn btn-primary py-3 px-4">Request a Free Consultation</a></p> -->
          </div>
        </div>
      </div>
    </div>
   <section class="ftco-section ftco-no-pb services-section"style="box-shadow: 5px 10px 18px #888888;">
      <div class="container" >
        <div class="row no-gutters d-flex" >

          <div class="col-md-3 text-center services align-self-stretch ftco-animate p-4" style="background:#2D2F3B;box-shadow: 0px 10px 0px #888888;">
            <div class="icon"><span >
				<img src="images/logos/apply.png" styel="height:1px;width:1px;"></img>
			</span></div>
            <div class="media-body">
              <h3 class="heading mb-3"><a href="services.html" style="color:#FDEC2A">Apply for our services	</a></h3>
              <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
            </div>
		  </div>
          <div class="col-md-3 text-center services align-self-stretch ftco-animate p-4" style="background:#17181D;" >
			<div class="icon"><span >
				<img src="images/logos/people.png" styel="height:1px;width:1px"></img>
			</span></div>
            <div class="media-body">
              <h3 class="heading mb-3"><a href="about.html" style="color:#FDEC2A">What we are about</a></h3>
              <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
            </div>
		  </div>
          <div class="col-md-3 text-center services align-self-stretch ftco-animate p-4"style="background:#2D2F3B;">
            <div class="icon"><span >
				<img src="images/logos/portfolio.png" styel="height:1px;width:1px"></img>
			</span></div>
            <div class="media-body">
              <h3 class="heading mb-3"><a href="portfolio.html" style="color:#FDEC2A">View our work</a></h3>
              <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
            </div>
		  </div>
		  <div class="col-md-3 text-center services align-self-stretch ftco-animate p-4"style="background:#17181D;">
            <div class="icon"><span >
				<img src="images/logos/contact.png" styel="height:1px;width:1px"></img>
			</span></div>
            <div class="media-body">
              <h3 class="heading mb-3"><a href="contact.html" style="color:#FDEC2A">Contact Us</a></h3>
              <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
            </div>
          </div>
        </div>
      </div>
	</section>

	<!-- <section class="ftco-section bg-light" style="box-shadow: 5px 10px 18px #888888;">
			<div class="row justify-content-center mb-5 pb-3" style="margin-top: -20px">
				<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
					<span class="subheading" style="color:grey">Our Services</span>
					<h2 style="color:#B7292D" class="mb-4">Here's What We Do</h2>
				</div>
			</div>
			<div class="myrow">
				<div class="servcolumn cover">
					<img src="images/architectureandinteriordesignservicepage/arch4.jpg" alt="Snow" class="myimage ftco-animate">
					<div class="middle">
							<a href="architecture.html" class="mytext ftco-animate" style="background: #B7292D;color: white;float:right;">Learn More</a>
					</div>
					<p style="text-align: center;color: #fff;position: absolute;left: 16px;bottom: 8px;">ARCHITECTURE AND INTERIOR DESIGN</p>
				</div>
	
				<div class="servcolumn cover">
					<img src="images/predes.jpg" alt="Snow" class="myimage ftco-animate">
					<div class="middle">
							<a href="pre_design.html" class="mytext ftco-animate" style="background: #B7292D;color: white;float:right;">Learn More</a>
					</div>
					<p style="text-align: center;color: #fff;position: absolute;left: 16px;bottom: 8px;">Pre Design</p>
				</div>
	
				<div class="servcolumn cover">
					<img src="images/susdesign/susdesign1.jpg" alt="Snow" class="myimage ftco-animate">
					<div class="middle">
							<a href="sus_design.html" class="mytext ftco-animate" style="background: #B7292D;color: white;float:right;">Learn More</a>
					</div>
					<p style="text-align: center;color: #fff;position: absolute;left: 16px;bottom: 8px;">Sustainable Design</p>
				</div>
	
				<div class="servcolumn cover">
					<img src="images/landscape/land1.jpg" alt="Snow" class="myimage ftco-animate">
					
					<p style="text-align: center;color: #fff;position: absolute;left: 16px;bottom: 8px;">Landscape Design</p>
				</div>
			</div>
		</section> -->

		<section class="ftco-section bg-light" style="box-shadow: 5px 10px 18px #888888;background: #d3d1d1">
			<div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
					<span class="subheading" style="color:grey">Our Services</span>
					<h2 style="color:black" class="mb-4">Here's What We Do</h2>
				</div>
			</div>

		<div class="mygridrow"> 
			<div class="mygridcolumn">
				<img src="images/architectureandinteriordesignservicepage/arch4.jpg" class="ftco-animate" style="object-fit: cover;height:450px;box-shadow: 5px 10px 18px #888888;">
				<a href="architecture.html" class="ftco-animate" style="text-align: center;color: rgb(255, 255, 255);font-size:18px; font-family:Poppins;position: absolute;left: 16px;bottom: 8px;">ARCHITECTURE AND INTERIOR DESIGN</a>
			</div>
			<div class="mygridcolumn">
				<img class="ftco-animate" src="images/susdesign/susdesign2.jpg" style="object-fit: cover;height:450px;box-shadow: 5px 10px 18px #888888;">
				<a href="sus_design.html" class="ftco-animate" style="text-align: center;color: rgb(255, 255, 255);font-size:18px;font-family:Poppins; position: absolute;left: 16px;bottom: 8px;">SUSTAINABLE DESIGN</a>
			</div>

			<div class="mygridcolumn">
				<img class="ftco-animate" src="images/predessss.jpg" style="object-fit: cover;height:450px;box-shadow: 5px 10px 18px #888888;">
				<a href="pre_design.html" class="ftco-animate" style="text-align: center;color: rgb(255, 255, 255);font-size:18px;font-family:Poppins;position: absolute;left: 16px;bottom: 8px;">PRE-DESIGN AND PLANNING</a>

			</div>

			<div class="mygridcolumn">
				<img class="ftco-animate" src="images/landscape/land2.jpg" style="object-fit: cover;height:450px;box-shadow: 5px 10px 18px #888888;">
				<a href="landscape_arch.html" class="ftco-animate" style="text-align: center;color: rgb(255, 255, 255);font-size:18px;font-family:Poppins;position: absolute;left: 16px;bottom: 8px;">LANDSCAPE DESIGN</a>

			</div>
		</div>	
		<br>	
	</section>
	
	<section class="ftco-section bg-light" style="box-shadow: 5px 10px 18px #888888;background: #d3d1d1">
			<div class="row justify-content-center mb-5 pb-3" style="margin-top: -50px">
					<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
						<span class="subheading" style="color:grey">Our Portfolio</span>
					  <h2 style="color:black" class="mb-4">View our amazing projects</h2>
					</div>
				  </div>
	<div class="gridrow"> 
		<div class="gridcolumn ftco-animate ">
			<img src="images/kikuyu/kikuyu1.jpg" style="width:100%">
			<img src="images/kikuyu/kikuyu2.jpg" style="width:100%">
			<img src="images/kikuyu/kikuyu3.jpg" style="width:100%">
			
		</div>
		<div class="gridcolumn ftco-animate">
			<img src="images/kithoka/kithoka1.jpg" style="width:100%">
			<img src="images/kithoka/kithoka2.jpg" style="width:100%">
			<img src="images/kithoka/kithoka3.jpg" style="width:100%">
			
		</div>
		<div class="gridcolumn ftco-animate">
			<img src="images/likoni/likoni1.jpg" style="width:100%">
			<img src="images/likoni/likoni2.jpg" style="width:100%">
			<img src="images/likoni/likoni3.jpg" style="width:100%">
			</div>
		
	</div>	
	<br>
	<a href="portfolio.html" class="mytext ftco-animate" style="background: #FDEC2A;font-family:poppins;color: black;float: right;">View all the projects</a>		

</section>

	<!-- <section class="ftco-section bg-light" style="box-shadow: 5px 10px 18px #888888;">
			<div class="row justify-content-center mb-5 pb-3">
					<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
						<span class="subheading" style="color:grey">Our Services</span>
						<h2 style="color:#B7292D" class="mb-4">View what we do</h2>
					</div>
				</div>
			<div class="row">
					<div class="column ftco-animate"  >
					  <div class="card" >
						<img src="images/architectureandinteriordesignservicepage/arch1.jpg" alt="Alps" style="width:100%;height:70%">
						<br>
						<h5 style="color:black;text-align: left"><b>Architecture and Interior Design</b></h4>
							<br>
						<p style="text-align: left;color:rgb(73, 69, 69);font-size: 15px">We provide architectural services for different buildingsWe provide architectural services for different \
							buildingsWe provide architectural services for different buildings</p>
					<br>
					<a href="architecture.html"  style="text-align: left" target="_self"><span><i class="fa fa-arrow-right"></i><b>Learn More</b></span></a>
				</div>
					</div>
				  
					<div class="column ftco-animate">
							<div class="card">
							  <img src="images/predessss.jpg" alt="Alps" style="width:100%;height:10%">
							  <br>
							  <h5 style="color:rgb(34, 29, 29);text-align: left"><b>Pre-Design and Planning</b></h4>
								<br>
							  <p style="text-align: left;color:rgb(73, 69, 69);font-size: 15px">We provide architectural services for different buildingsWe provide architectural services for different \
								  buildingsWe provide architectural services for different buildings
						  </p>
						  <br>
						  <a href="pre_design.html"  style="text-align: left" target="_self"><span><i class="fa fa-arrow-right"></i><b>Learn More</b></span></a>
							</div>
						  </div>
					
						  <div class="column ftco-animate" style="height: 400px">
								<div class="card">
								  <img src="images/susdesign.jpg" alt="Alps" style="width:100%;height:70%">
								  <br>
								  <h5 style="color:rgb(34, 29, 29);text-align: left"><b>Sustainable Design</b></h4>
									<br>
								  <p style="text-align: left;color:rgb(73, 69, 69);font-size: 15px">We provide architectural services for different buildingsWe provide architectural services for different \
									  buildingsWe provide architectural services for different buildings
							  </p>
							  <br>
							  <a href="sus_design.html"  style="text-align: left" target="_self"><span><i class="fa fa-arrow-right"></i><b>Learn More</b></span></a>
								</div>
							  </div>

							  <div class="column ftco-animate" style="height: 400px;padding-bottom: 30px">
									<div class="card">
									  <img src="images/landscapedesign.jpg" alt="Alps" style="width:100%;height:70%">
									  <br>
									  <h5 style="color:rgb(34, 29, 29);text-align: left"><b>Landscape Design</b></h4>
										<br>
									  <p style="text-align: left;color:rgb(73, 69, 69);font-size: 15px">We provide architectural services for different buildingsWe provide architectural services for different \
										  buildingsWe provide architectural services for different buildings
								  </p>
								  <br>
								  <a href="landscape_arch.html"  style="text-align: left" target="_self"><span><i class="fa fa-arrow-right"></i><b>Learn More</b></span></a>
									</div>
								  </div>
				  
	</section> -->


	<section class="ftco-consultation bg-light" style="box-shadow: 5px 10px 18px #888888;">
    		<div class="container-fluid">
					<br>
					<br>
    		<div class="row d-md-flex">
    			<div class="half d-flex justify-content-center align-items-center img" style="background-image: url(images/slideshow/back6.jpg);">
    				<div class="overlay"></div>
						<div class="desc text-center">
							<div>
								<span>
									<img src="images/logos/archi2.png" style="height:100px;width:100px;"></img>
								</span>
							</div>
							<h1><a href="index.php" style="color:white">Architrevo <br><span>Inspiring Better Living</span></a></h1>
						</div>
    				</div>
    				<div class="half p-3 p-md-5 ftco-animate">
    					<h3 class="mb-4">Ask us a question!</h3>
    					<form action="#" style="font-family:poppins">
							<div class="form-group">
							<input type="text" class="form-control" placeholder="Your Name">
							</div>
							<div class="form-group">
							<input type="text" class="form-control" placeholder="Your Email">
							</div>
							<div class="form-group">
							<input type="text" class="form-control" placeholder="Subject">
							</div>
							<div class="form-group">
							<textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
							</div>
							<div class="form-group">
								<input type="submit" value="Send message" class="btn btn-primary" style="color:black">
							</div>
	          			</form>
    			</div>
    		</div>
    	</div>
    </section>
<hr>

	<section class="ftco-section bg-light">
			<div class="container">
			  <div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 heading-section text-center ftco-animate">
					<span class="subheading">Our Blog</span>
				  <h2>Recent Blog</h2>
				</div>
			  </div>
			  <div class="row d-flex">
				  <?php
				  require($_SERVER['DOCUMENT_ROOT'] . '/mirabile/mirabile/wordpress/wordpress/wp-load.php');
				  $args = array(
				  // 'cat' => 3, // Only source posts from a specific category
				  'posts_per_page' => 3 // Specify how many posts you'd like to display
				  );
				  $latest_posts = new WP_Query( $args );
				  if ( $latest_posts->have_posts() ) {
				  while ( $latest_posts->have_posts() ) {
				  $latest_posts->the_post(); 
				?>
				  <div class="col-md-4 d-flex ftco-animate">
					  <div class="blog-entry justify-content-end">
					  <?php if ( has_post_thumbnail() ) { ?>
						<span class="block-20" style="object-fit:cover"><?php the_post_thumbnail(); ?></span>
						<?php } ?>
						<div class="text p-4 float-right d-block" style="font-family:poppins">
						  <div class="topper d-flex align-items-center">
							<div class="one py-2 pl-3 pr-1 align-self-stretch">
								<span class="day" style="font-size:30px;font-family:poppins;color:black;"><?php the_time('jS') ?></span>
							</div>
							
								<div class="two pl-0 pr-3 py-2 align-self-stretch" >
									<span class="yr" style="font-family:poppins;color:black;"><?php the_time('F') ?></span>
									<span class="mos" style="font-family:poppins;color:black;"><?php the_time('Y') ?></span>
								</div>
						  </div>
						  <h3 class="heading mt-2" style="font-family:poppins"><a target="_blank" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						  <p style="font-family:poppins"><?php the_excerpt(); ?></p>
						</div>
					  </div>
					</div>
			  
			  <?php }
				  } else {
				  echo '<p>There are no posts available</p>';
				  }
				  wp_reset_postdata();
				  ?>
	  </div>
			</div>
		  </section>
		
	<section class="ftco-section-parallax bg-secondary">
			<div class="parallax-img d-flex align-items-center">
			  <div class="container">
				<div class="row d-flex justify-content-center">
				  <div class="col-md-7 text-center heading-section heading-section-white ftco-animate" style="font-family:poppins">
					<h2>Subcribe to our Newsletter</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
					<div class="row d-flex justify-content-center mt-4 mb-4">
					  <div class="col-md-8">
						<form action="#" class="subscribe-form">
						  <div class="form-group d-flex">
							<input type="text" class="form-control" placeholder="Enter email address">
							<input type="submit" value="Subscribe" class="submit px-3" style="color:black;">
						  </div>
						</form>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		  </section>
	
		  <footer class="ftco-footer ftco-section" style="background: #17181D">
				<div class="container">
				  <div class="row mb-5">
				  <div class="col-md">
					<div class="ftco-footer-widget mb-4" style="font-family:poppins">
					<h2 class="ftco-heading-2 ftco-animate">Follow us on social media</h2>
					<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
							<li class="ftco-animate"><a href="https://twitter.com/marchdesigns" target="_blank"><span> <img src="images/madtwit2.png"></img> </span></a></li>
							<li class="ftco-animate"><a href="https://www.linkedin.com/company/mirabile-archdesigns" target="_blank"><span> <img src="images/linked.png"></img> </span></a></li>
							<li class="ftco-animate"><a href="https://www.facebook.com/MirabileArchitects/?ref=br_rs" target="_blank"><span><img src="images/madfb2.png"></img> </span></a></li>
							<li class="ftco-animate"><a href="https://www.youtube.com/channel/UCnKW-xjadWsUhP6xvWbDQ2w" target="_blank"><span><img src="images/youtube.png"></img> </span></a></li>
					  <li class="ftco-animate"><a href="https://www.instagram.com/mirabile_archdesigns/?hl=en" target="_blank"><span> <img src="images/insta.png"></img> </span></a></li>
					</ul>
					</div>
				  </div>
				  <div class="col-md">
					<div class="ftco-footer-widget mb-4 ml-md-5" style="font-family:poppins">
					<h2 class="ftco-heading-2 ftco-animate">Our Services</h2>
					<ul class="list-unstyled">
					  <li><a href="architecture.html" style="color:white" class="py-1 d-block ftco-animate"><span class="ion-ios-arrow-forward mr-3" style="color: white"></span>Architecture & Interior Design</a></li>
					  <li><a href="pre_design.html" style="color:white" class="py-1 d-block ftco-animate"><span class="ion-ios-arrow-forward mr-3" style="color: white"></span>Pre Design & Planning</a></li>
					  <li><a href="sus_design.html" style="color:white" class="py-1 d-block ftco-animate"><span class="ion-ios-arrow-forward mr-3" style="color: white"></span>Sustainable Design</a></li>
					  <li><a href="landscape_arch.html" style="color:white" class="py-1 d-block ftco-animate"><span class="ion-ios-arrow-forward mr-3"style="color: white" ></span>Landscape Architecture</a></li>
			  
					</ul>
					</div>
				  </div>
				  
				  <div class="col-md">
					<div class="ftco-footer-widget mb-4" style="font-family:poppins">
					  <h2 class="ftco-heading-2 ftco-animate">Have a Question?</h2>
					  <div class="block-23 mb-3">
					  <ul>
						<li><span class="icon icon-map-marker ftco-animate"></span><span class="text ftco-animate" style="color: white">P.O.Box: 14 - 00101. Juja, Kenya</span></li>
						<li><span><img class="ftco-animate" src="images/madphone.png"></img></span><span class="text ftco-animate" style="color:white"> +254 729 038 088</span></li>
						<li><span><img class="ftco-animate" src="images/madphone.png"></img></span><span class="text ftco-animate" style="color:white"> +254 738 920 749</span></li>
						<li><span><img class="ftco-animate" src="images/madphone.png"></img></span><span class="text ftco-animate" style="color:white"> +254 706 457 150</span></li>
						<li><span><img class="ftco-animate" src="images/madevn.png"></img></span>&nbsp;&nbsp;<span class="text ftco-animate" style="color:white"> info@architrevo.com</span></li>
					  </ul>
					  </div>
					</div>
				  </div>
				  </div>
				  <div class="row">
				  <div class="col-md-12 text-center">
			  
					<p class="ftco-animate">
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </a>
			  </p>
				  </div>
				  </div>
				</div>
				</footer>
		  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <script>
	window.onscroll = function() {myFunction()};
	
	var navbar = document.getElementById("ftco-navbar");
	var sticky = navbar.offsetTop;
	
	function myFunction() {
	  if (window.pageYOffset >= sticky) {
		navbar.classList.add("sticky")
	  } else {
		navbar.classList.remove("sticky");
	  }
	}
	</script>
	<a target="_blank" href="https://icons8.com/icons/set/moleskine">Moleskine icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>