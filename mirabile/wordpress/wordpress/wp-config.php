<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mirabile' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{8YPX_6Px.7^>9mAGVDioX4Q@Ox!7rZ4rN+S6|,WE|{Yf<xORy;&bEGTJ24O*@#C' );
define( 'SECURE_AUTH_KEY',  'O)X0*TF_zv5ma_V37d1=,*7NWUVeatQ8B$5:CY7@]jZh<aXXo6jQ<cbra& $vgSI' );
define( 'LOGGED_IN_KEY',    'Up77(|4DI(=FkS&,%LZ&XLf(@8W:+/Ia~g8YFGTzCC!8[F;Mn)frE4AC7;_3%|6c' );
define( 'NONCE_KEY',        '2OVtRO`RlOXYS$)yJw*W;gPHN5k,RT$mc#@>!RFn9T:]WMj!,xzP/jlthqzYhOGl' );
define( 'AUTH_SALT',        'U)w?_dj)m}FugDa$99lDMt|/sMZ`5Hh`tuGuFPI]0[Pb[E:7DhNC/=Eua_6t9CQx' );
define( 'SECURE_AUTH_SALT', '&bl:yw(43XWY<Rhk9SIrR:OCKV2N+HpdJat=2KSdztJqkejC_ 43vP<Wo}G5N7 s' );
define( 'LOGGED_IN_SALT',   '_VR9X9#F]TOY`CRK/|Q{Bj.OK^?Lh8QRr}GQW(y~T[U<pWDQU|HJF?M^tmmAZd*R' );
define( 'NONCE_SALT',       'UmhOu57m/sUup KX%mO$G?lq<QQ?e=Fx|@e!ugs<J4@V(w}gjJ~SjT7j>xuY*e6%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'table_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
